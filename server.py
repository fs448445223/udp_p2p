#!/usr/bin/python3

import threading, socket

class MyServer:
    
    def __init__(self, port):
        self.serverSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.serverSocket.bind(('0.0.0.0', port))

        self.retryCount = 0
        self.status = 'before A connect'
        self.addrA = ('', 0)
        self.addrB = ('', 0)

        self.testAddrA()
        while (True):
            self.run()

    def testAddrA(self):
        threading.Timer(1, self.testAddrA).start()
        if (self.addrA != ('', 0)):
            print(self.retryCount)
            if (self.retryCount > 3):
                self.status = 'before A connect'
                self.addrA = ('', 0)
                self.retryCount = 0
                print('before A connect')
            self.retryCount += 1

    def replyEmpty(self):
        self.serverSocket.sendto(b'0', self.addrA)

    def replyAddress(self, data, addr):
        temp = '{}:{}'.format(data[0], data[1])
        self.serverSocket.sendto(temp.encode(encoding='utf-8'), addr)

    def run(self):
        data, addr = self.serverSocket.recvfrom(1024)
        print(addr)
        if (self.status == 'before A connect'):
            self.addrA = addr
            self.status = 'before B connect'
            self.replyEmpty()
        elif (self.status == 'before B connect'):
            if (addr == self.addrA):
                self.retryCount = 0
                self.replyEmpty()
            else:
                self.status = 'after B connect'
                self.replyAddress(self.addrA, addr)
        elif (self.status == 'after B connect'):
            if (addr == self.addrA):
                self.replyAddress(self.addrB, addr)
            elif (addr == self.addrB):
                self.replyAddress(self.addrA, addr)
        print(self.status)

test = MyServer(12345)

import socket, threading, time

class MyClient:

    def __init__(self, addr):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.addr = addr
        self.addrA = ('', 0)

        self.status = 'Before server'
        self.retry = 0
        self.run()

    def getAddr(data):
        temp = data.decode().split(':')
        return (temp[0], int(temp[1]))

    def run(self):
        threading.Timer(1, self.run).start()
        if (self.status == 'Before server'):
            self.socket.sendto(b'0', self.addr)
            data, addr = self.socket.recvfrom(64)
            if (data == b'0'):
                self.status = 'After server'
            else:
                print(data)
                self.status = 'To connect'
                self.addrA = MyClient.getAddr(data)
        elif (self.status == 'After server'):
            self.socket.sendto(b'-1', self.addr)
            data, addr = self.socket.recvfrom(64)
            self.retry = 0
            if (data != b'0'):
                print(data)
                self.status = 'Before connected'
                self.addrA = MyClient.getAddr(data)
        elif (self.status == 'To connect'):
            self.socket.sendto(b'Hello, A', self.addrA)
            data, addr = self.socket.recvfrom(64)
            if (addr == self.addrA):
                print(data)
        elif (self.status == 'Before connected'):
            data, addr = self.socket.recvfrom(64)
            if (addr == self.addrA):
                print(data)
                self.socket.sendto(b'Hello, B', self.addrA)

class MyTransmission:

    def __init__(self):
        self.toAck = 1

    def parseData(data):
        seq = int.from_bytes(data[0:4])
        return (seq, data[4, -1])

    def receive(data):
        seq, data = MyTransmission.parseData(data)
        if (seq == self.toAck):
            self.toAck += 1
            return (self.toAck, data)
        return (0, b'')

client = MyClient(('120.76.239.11', 12345))
